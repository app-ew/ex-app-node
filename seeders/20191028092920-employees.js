'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    return queryInterface.bulkInsert('employees', [{
      firstName: 'firstName 1',
      lastName: 'lastName 1',
      departmentId: 1,
      createdAt: new Date()
    },{
      firstName: 'firstName 2',
      lastName: 'lastName 2',
      departmentId: 1,
      createdAt: new Date()
    },{
      firstName: 'firstName 3',
      lastName: 'lastName 3',
      departmentId: 3,
      createdAt: new Date()
    },{
      firstName: 'firstName 4',
      lastName: 'lastName 4',
      departmentId: 2,
      createdAt: new Date()
    },], {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};
