'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */

    return queryInterface.bulkInsert('departments', [{
      name: 'Marketing',
      description: 'Marketing description',
      createdAt: new Date()
    },{
      name: 'Support',
      description: 'Support description',
      createdAt: new Date()
    },{
      name: 'Accounting',
      description: 'Accounting description',
      createdAt: new Date()
    },{
      name: 'General',
      description: 'General description',
      createdAt: new Date()
    },{
      name: 'Administrative',
      description: 'Administrative description',
      createdAt: new Date()
    },], {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
    return queryInterface.bulkDelete('departments', null, {});
  }
};
