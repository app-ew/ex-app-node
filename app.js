const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');

// This will be our application entry. We'll setup our server here.
const http = require('http');
const cors = require('cors');

// Set up the express app
const app = express();

// Log requests to the console.
app.use(logger('dev'));

// Parse incoming requests data (https://github.com/expressjs/body-parser)
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(cors());

//Models
var models = require('./models')
models.sequelize.sync()
    .then(() => { console.log('Test connection is Ok') })
    .catch((err) => { console.log({ err }) })

require('./routes')(app)

// Setup a default catch-all route that sends back a welcome message in JSON format.
app.get('/favicon.ico', (req, res) => res.status(200).send(''));
app.get('*', (req, res) => res.status(200).send({
    message: 'Welcome to the beginning of nothingness.',
}));

const port = parseInt(process.env.PORT, 10) || 3000;
app.set('port', port);
const server = http.createServer(app);

server.listen(port);
module.exports = app;
