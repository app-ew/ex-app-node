const Department = require('./Department')
const Employee = require('./Employee')

module.exports = {
    Department,
    Employee
}