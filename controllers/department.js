const Department = require("../models").Department

module.exports = {
    create(req, res) {
        return Department.create(req.body)
            .then(item => res.status(200).send({ result: 1, item }))
            .catch(error => res.status(500).send({ result: 0, error }))
    },
    find(req, res) {
        return Department.findOne({
            where: {
                id: req.params.id
            }
        })
            .then(item => res.status(200).send({ result: 1, item }))
            .catch(error => res.status(500).send({ result: 0, error }))
    },
    all(req, res) {
        return Department.findAll()
            .then(items => res.status(200).send({ result: 1, items }))
            .catch(error => res.status(500).send({ result: 0, error }))
    },
    update(req, res) {
        return Department.update({
            ...req.body,
            updatedAt: new Date(),
        }, {
            where: {
                id: req.body.id
            }
        })
            .then(item => res.status(200).send({ result: 1 }))
            .catch(error => res.status(500).send({ result: 0, error }))
    },
    remove(req, res) {
        return Department.destroy({
            where: {
                id: req.params.id
            }
        })
            .then(item => res.status(200).send({ result: 1 }))
            .catch(error => res.status(500).send({ result: 0, error }))
    },
}