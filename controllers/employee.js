const Employee = require("../models").Employee

module.exports = {
    create(req, res) {
        return Employee.create(req.body)
            .then(item => res.status(200).send({ result: 1, item }))
            .catch(error => res.status(500).send({ result: 0, error }))
    },
    find(req, res) {
        return Employee.findOne({
            where: {
                id: req.params.id
            }
        })
            .then(item => res.status(200).send({ result: 1, item }))
            .catch(error => res.status(500).send({ result: 0, error }))
    },
    all(req, res) {
        return Employee.findAll()
            .then(items => res.status(200).send({ result: 1, items }))
            .catch(error => res.status(500).send({ result: 0, error }))
    },
    update(req, res) {
        return Employee.update({
            ...req.body,
            updatedAt: new Date(),
        }, {
            where: {
                id: req.body.id
            }
        })
            .then(item => res.status(200).send({ result: 1 }))
            .catch(error => res.status(500).send({ result: 0, error }))
    },
    remove(req, res) {
        return Employee.destroy({
            where: {
                id: req.params.id
            }
        })
            .then(item => res.status(200).send({ result: 1 }))
            .catch(error => res.status(500).send({ result: 0, error }))
    },
}