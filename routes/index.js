const ControllerDepartment = require('../controllers').Department
const ControllerEmployee = require('../controllers').Employee

module.exports = (app) => {
    app.get('/api', (req, res) => res.status(200).send({message: 'hello'}))

    app.get('/api/Department', (req, res) => ControllerDepartment.all(req,  res))
    app.get('/api/Department/:id', (req, res) => ControllerDepartment.find(req,  res))
    app.put('/api/Department', (req, res) => ControllerDepartment.create(req, res))
    app.post('/api/Department', (req, res) => ControllerDepartment.update(req, res))
    app.delete('/api/Department/:id', (req, res) => ControllerDepartment.remove(req, res))

    app.get('/api/Employee', (req, res) => ControllerEmployee.all(req,  res))
    app.get('/api/Employee/:id', (req, res) => ControllerEmployee.find(req,  res))
    app.put('/api/Employee', (req, res) => ControllerEmployee.create(req, res))
    app.post('/api/Employee', (req, res) => ControllerEmployee.update(req, res))
    app.delete('/api/Employee/:id', (req, res) => ControllerEmployee.remove(req, res))

}