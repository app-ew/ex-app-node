'use strict';
module.exports = (sequelize, DataTypes) => {
  const Employee = sequelize.define('Employee', {
    firstName: {
      type: DataTypes.STRING,
      defaultValue: false
    },
    lastName: {
      type: DataTypes.STRING,
      defaultValue: false
    },
  }, {});
  Employee.associate = function(models) {
    // associations can be defined here
    Employee.belongsTo(models.Department, {
      foreignKey: 'departmentId',
      onDelete: 'CASCADE'
    })
  };
  return Employee;
};